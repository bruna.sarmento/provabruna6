
def boryMassIndex(height, weight):
    bmi = weight/height**2
    print("IMC = ",round(bmi,2))
    return bmi

def finalConditionBoryMassIndex(bmi):
    if bmi <= 17:
        print('Situação: Muito abaixo do peso')
    elif 17.1 < bmi <= 18.5:
        print('Situação: Abaixo do peso')
    elif 18.6 < bmi <= 24.9:
        print('Situação: Peso normal')
    elif 25 < bmi <= 29.9:
        print('Situação: Acima do peso')        
    elif 30 < bmi <= 34.9:
        print('Situação: Obesidade grau I')     
    elif 35 < bmi <= 39.9:
        print('Situação: Obesidade grau II')        
    else:
        print('Situação: Obesidade grau III')
    